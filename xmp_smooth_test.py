import unittest

from xmp_smooth import smooth_array, num_from_file_name

class TestXmpSmooth(unittest.TestCase):
    def test_num_from_file_name(self):
        self.assertEqual(num_from_file_name('IMG_0001.JPG'), 1)
        self.assertEqual(num_from_file_name('IMG_0100.JPG'), 100)
        self.assertEqual(num_from_file_name(''), 0)
        self.assertEqual(num_from_file_name('123_456.jpg'), 0)

    def test_smooth_array(self):
        self.assertEqual(
            smooth_array(cnt=10, start=.0, end=10.),
            [0.0, 1.11, 2.22, 3.33, 4.44, 5.56, 6.67, 7.78, 8.89, 10.0]
        )
        self.assertEqual(
            smooth_array(cnt=10, start=10., end=.0),
            [10.0, 8.89, 7.78, 6.67, 5.56, 4.44, 3.33, 2.22, 1.11, 0.0]
        )
        self.assertEqual(
            smooth_array(cnt=10, start=10., end=10.),
            [10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
        )
        self.assertEqual(
            smooth_array(cnt=18, start=-.30, end=1.5),
            [-0.3, -0.19, -0.09, 0.02, 0.12, 0.23, 0.34, 0.44, 0.55, 0.65, 0.76, 0.86, 0.97, 1.08, 1.18, 1.29, 1.39, 1.5]
        )
        self.assertEqual(
            smooth_array(cnt=19, start=-1.35, end=0.3),
            [-1.35, -1.26, -1.17, -1.07, -0.98, -0.89, -0.8, -0.71, -0.62, -0.52, -0.43, -0.34, -0.25, -0.16, -0.07, 0.03, 0.12, 0.21, 0.3]
        )
