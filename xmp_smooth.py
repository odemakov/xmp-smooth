#!/usr/local/bin/python

import argparse
import re
import os
import xml.etree.ElementTree as ET

PWD = os.getcwd()
CRS_TAGS_DESCRIPTION = ['Temperature', 'Tint', 'Exposure2012']
CRS_TAGS_REGEXP = ['Top', 'Left', 'Bottom', 'Right', 'Angle']


def main():
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('--start', type=str, nargs='?', help='start file')
    parser.add_argument('--end', type=str, nargs='?', help='end file')
    parser.add_argument('--smooth', type=float, nargs='?', help='Smooth parameter to this value')
    parser.add_argument('--overwrite', default=False, help='Overwrite existing .xmp files', action='store_true')
    parser.add_argument('--tag', choices=CRS_TAGS_DESCRIPTION + CRS_TAGS_REGEXP, help='Tag to process')
    args = parser.parse_args()

    if args.tag not in CRS_TAGS_DESCRIPTION + CRS_TAGS_REGEXP:
        parser.print_help()
        exit(1)

    xml_files = get_xmp_files(args.start, args.end)

    # check main exposure settings, we don't wont smooth values
    # between different exposure settings
    check_exposure_settings(xml_files)

    # read value of x:xmpmeta -> rdf:RDF -> rdf:Description - attr
    # crs:Exposure2012 from first and last files
    if args.smooth:
        process_tag_smooth(xml_files, args.smooth, args.tag, args.overwrite)
        pass
    else:
        process_tag(xml_files, args.tag, args.overwrite)

def check_exposure_settings(files):
    check_old = None
    file_old = None
    for file_current in files:
        tree = ET.parse(file_current)
        root = tree.getroot()
        rdf = root.find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}RDF')
        description = rdf.find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description')
        exposure = description.get('{http://ns.adobe.com/exif/1.0/}ExposureTime')
        f = description.get('{http://ns.adobe.com/exif/1.0/}FNumber')
        isos = description.find('{http://ns.adobe.com/exif/1.0/}ISOSpeedRatings').find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Seq')
        iso = isos.find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}li')
        check_current = '{}_{}_{}'.format(exposure, f, iso.text)
        if check_old is None:
            check_old = check_current
        elif check_old != check_current:
            print("Different exposure_f_iso settings in {}({}) {}({}). Check your input.".format(
                file_old, check_old,
                file_current, check_current
            ))
            exit(1)
        file_old = file_current

def process_tag_smooth(xml_files, smooth, tag, overwrite):
    """
    Read value from file and change it accordingly smoothed value
    """
    smooth_values = smooth_array(
        cnt=len(xml_files),
        start=.0,
        end=smooth,
        precision=(6 if tag in CRS_TAGS_REGEXP else 2)
    )
    for i in range(len(xml_files)):
        item = xml_files[i]
        value = smooth_values[i]
        f = open(os.path.join(PWD, item), 'r+')
        content = f.read()
        f.close()
        old_value = re.findall(
            r'crs:%s="([\d\.\-\+]+)"' % tag,
            content
        )
        if tag == 'Exposure2012':
            old_value = float(old_value[0])
            new_value = '%+0.2f' % (old_value + value);
        elif tag == 'Temperature':
            old_value = int(old_value[0])
            new_value = str(old_value + int(value));
        elif tag == 'Tint':
            old_value = int(old_value[0])
            new_value = '{0:+d}'.format(old_value + int(value));
        elif tag == 'Top':
            old_value = float(old_value[0])
            new_value = '%+0.6f' % (old_value + value);
        write_content(item, tag, new_value, content, overwrite)

def process_tag(xml_files, tag, overwrite):
    """
    Read values from first and last images and smooth them linearly for intermediate images
    """
    if tag in CRS_TAGS_DESCRIPTION:
        value_start, value_end = get_value_from_description(xml_files[0], tag), get_value_from_description(xml_files[len(xml_files)-1], tag)
    else:
        value_start, value_end = get_value_by_regexp(xml_files[0], tag), get_value_by_regexp(xml_files[len(xml_files)-1], tag)

    if value_start == value_end:
        print("Start and end values are identical({}), skip processing".format(str(value_start)))
    else:
        smooth_values = smooth_array(
            cnt=len(xml_files),
            start=value_start,
            end=value_end,
            precision=(6 if tag in CRS_TAGS_REGEXP else 2)
        )
        if tag == 'Exposure2012':
            for i in range(len(smooth_values)):
                smooth_values[i] = '%+0.2f' % smooth_values[i]
        elif tag == 'Temperature':
            for i in range(len(smooth_values)):
                smooth_values[i] = str(int(smooth_values[i]))
        elif tag == 'Tint':
            for i in range(len(smooth_values)):
                smooth_values[i] = '{0:+d}'.format(int(smooth_values[i]))
        elif tag == 'Top':
            for i in range(len(smooth_values)):
                smooth_values[i] = '%+0.6f' % smooth_values[i]
        for i in range(len(xml_files)):
            item = xml_files[i]
            value = smooth_values[i]
            print(item, tag, value)
            f = open(os.path.join(PWD, item), 'r')
            content = f.read()
            f.close()
            write_content(item, tag, value, content, overwrite)

def write_content(item, tag, value, content, overwrite):
    new_content = re.sub(
        r'crs:%s="[\d\.\-\+]+"' % tag,
        'crs:%s="%s"' % (tag, value),
        content
    )
    if tag == 'Temperature':
        new_content = re.sub(
            r'crs:WhiteBalance="[\w]+"',
            'crs:WhiteBalance="Custom"',
            new_content
        )
    f = open(os.path.join(
        PWD,
        ("%s" % item) if overwrite else ("%s.tmp" % item)
    ), 'w')
    f.write(new_content)
    f.close()

def smooth_array(cnt, start, end, precision = 2):
    if start == end:
        return [cnt for i in range(cnt)]

    #print(cnt, start, end)
    diff = max(start, end) - min(start, end)
    step = diff / (cnt - 1)
    #print(diff, step)
    result = []
    curr = start
    if start < end:
        while(curr < end):
            result.append(round(curr, precision))
            curr += step
    else:
        while(curr > end):
            result.append(round(curr, precision))
            curr -= step

    # append last result in case of 'round' lack
    if len(result) < cnt:
        result.append(end)

    return result
    
def get_value_from_description(name, tag):
    tree = ET.parse(name)
    root = tree.getroot()
    rdf = root.find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}RDF')
    '''
    xmlns:tiff="http://ns.adobe.com/tiff/1.0/"
    xmlns:exif="http://ns.adobe.com/exif/1.0/"
    xmlns:aux="http://ns.adobe.com/exif/1.0/aux/"
    xmlns:xmp="http://ns.adobe.com/xap/1.0/"
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/"
    xmlns:stEvt="http://ns.adobe.com/xap/1.0/sType/ResourceEvent#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xmpRights="http://ns.adobe.com/xap/1.0/rights/"
    xmlns:Iptc4xmpCore="http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/"
    xmlns:crs="http://ns.adobe.com/camera-raw-settings/1.0/"
    '''
    description = rdf.find('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description')
    return float(description.get('{http://ns.adobe.com/camera-raw-settings/1.0/}%s' % tag))
    
def get_value_by_regexp(name, tag):
    f = open(name, 'r')
    m = re.findall("crs\:%s=\"([\d\.\-\+]+)\"" % tag, f.read())
    if m: return float(m[0])
    else: return None

def get_xmp_files(start_file, end_file):
    start, end = num_from_file_name(start_file), num_from_file_name(end_file)
    print(start, end)
    result = []
    for item in os.listdir(PWD):
        if os.path.isfile(os.path.join(PWD, item)) and item.endswith('.xmp'):
            number = num_from_file_name(item)
            if number > 0 and number >= start and number <= end:
                result.append(item)
    return sorted(result)

def num_from_file_name(name):
    r = re.findall(r'\d+', name)
    if len(r) == 1:
        return int(r[0])
    else:
        return 0

if __name__ == "__main__":
    main()
