VIRTUAL_ENV      = source venv/bin/activate

# -- Rules
default: help

init: ## init environment
	@python3 -mvenv venv
	$(VIRTUAL_ENV) && pip install -r requirements/dev.txt
	@echo To activate venv run 'source venv/bin/activate' command.
	@echo Or you can use built-in make targets, run 'make help' to show all of them.
.PHONY: init

test: ## run test
	$(VIRTUAL_ENV) && python -m unittest discover . -p '*_test.py'
.PHONY: test

clean: ## restore repository state as it was freshly cloned
	@git clean -idx
.PHONY: clean


help:
	@grep -h -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help
