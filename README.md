### Smooth Exposure2012/Temperature tags in .xmp files

### Script usage

Use this script with Adobe Lightroom Classic. Enable `Automatically
writing changes into XMP` in  `Catalog Settings` -> `Metadata`. 

This simple script have 2 modes:

#### 1. Running without `--smooth` parameter
It takes values from first and last .xmp files and distributes them
linearly for intermediate images. Basically using to smooth
explosure/temperature part of images with same explosure value.

`python xmp-smooth.py --start IMG_0000.xmp --end IMG_0100.xmp --tag
Exposure2012`

Let's IMG_0000.xmp has 'Exposure2012' value = 5500 and IMG_0100.xmp
= 6500. This command will set 'Exposure2012' in such manner:

- IMG_0000.xmp = 5500
- IMG_0001.xmp = 5510
- IMG_0001.xmp = 5520
- ...
- IMG_0099.xmp = 6490
- IMG_0100.xmp = 6500

#### 2. Running with `--smooth` parameter
It calculates values from 0 to 'smooth' value, and apply this values
to each image.

`python xmp-smooth.py --start IMG_0000.xmp --end IMG_0100.xmp --tag
Temperature --smooth 100`

With this command each image from 0000 to 0100 will incease
'Temperature' tag value in such manner:

- IMG_0000.xmp +0
- IMG_0001.xmp +1
- IMG_0002.xmp +1
- ...
- IMG_0099.xmp +99
- IMG_0100.xmp +100

`--overwrite` flag will actually do the job, without it script will
create .tmp file for each .xmp to check result.

### Create final video

`ffmpeg -r 24 -pattern_type glob -i '*.jpg' -s hd1080 -vcodec
libx264 -crf 18 -preset slow timelapse.mp4`
